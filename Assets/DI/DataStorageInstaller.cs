using DataStorage;
using UnityEngine;
using Zenject;

public class DataStorageInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        IDataStorage dataStorage = GetComponentInChildren<IDataStorage>();

        Container.Bind<IDataStorage>().To(dataStorage.GetType()).FromInstance(FindObjectOfType(dataStorage.GetType())).AsSingle();
    }
}