﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Card", menuName = "Card")]
public class Card : ScriptableObject
{
    public Sprite sprite { get => _sprite; private set => _sprite = value; }

    [SerializeField]
    private Sprite _sprite;
}