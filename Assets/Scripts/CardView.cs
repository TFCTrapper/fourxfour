﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class CardView : MonoBehaviour, IInitializable, IKillable, ISelectable
{
    public Card card { get => _card; private set => _card = value; }
    public Vector2Int position { get; private set; }

    [SerializeField]
    private SpriteRenderer _imageRenderer;
    [SerializeField]
    private Card _card;
    [SerializeField]
    private SpriteRenderer _frameRenderer;

    private Game _game;

    public void Initialize(dynamic data)
    {
        _game = data.game;
        _card = data.card;
        position = data.position;

        _imageRenderer.sprite = _card.sprite;
    }

    public void Kill()
    {
        _game.fieldController.field[position.x][position.y] = null;
        Destroy(gameObject);
    }

    public void Select()
    {
        _frameRenderer.color = _game.selectedCardColor;
        _game.fieldController.selectedCard = this;
    }

    public void Unselect()
    {
        _frameRenderer.color = _game.defaultCardColor;
        _game.fieldController.selectedCard = null;
    }

    private void OnMouseDown()
    {
        if (_game.fieldController.selectedCard == null)
        {
            Select();
            return;
        }
        if (_game.fieldController.selectedCard.Equals(this))
        {
            Unselect();
            return;
        }
        if (_game.fieldController.selectedCard.card.Equals(card))
        {
            _game.Match(_game.fieldController.selectedCard, this);
            return;
        }
        _game.fieldController.selectedCard.Unselect();
        Select();
    }
}