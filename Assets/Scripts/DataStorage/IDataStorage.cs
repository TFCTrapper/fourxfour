﻿namespace DataStorage
{
    public interface IDataStorage
    {
        void SetValue(string key, int value);
        void SetValue(string key, float value);
        void SetValue(string key, string value);
        void SetValue(string key, bool value);
        string GetString(string key);
        int GetInt(string key);
        float GetFloat(string key);
        bool GetBool(string key);
        bool HasKey(string key);
        void DeleteKey(string key);
    }
}
