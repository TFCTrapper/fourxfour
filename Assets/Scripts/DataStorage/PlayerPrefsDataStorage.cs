﻿using UnityEngine;

namespace DataStorage
{
    public class PlayerPrefsDataStorage : MonoBehaviour, IDataStorage
    {
        public void SetValue(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
        }

        public void SetValue(string key, float value)
        {
            PlayerPrefs.SetFloat(key, value);
        }

        public void SetValue(string key, string value)
        {
            PlayerPrefs.SetString(key, value);
        }

        public void SetValue(string key, bool value)
        {
            int intValue = value ? 1 : 0;
            PlayerPrefs.SetInt(key, intValue);
        }

        public string GetString(string key)
        {
            return PlayerPrefs.GetString(key);
        }

        public int GetInt(string key)
        {
            return PlayerPrefs.GetInt(key);
        }

        public float GetFloat(string key)
        {
            return PlayerPrefs.GetFloat(key);
        }

        public bool GetBool(string key)
        {
            int intValue = PlayerPrefs.GetInt(key);
            return intValue == 1;
        }

        public bool HasKey(string key)
        {
            return PlayerPrefs.HasKey(key);
        }

        public void DeleteKey(string key)
        {
            PlayerPrefs.DeleteKey(key);
        }
    }
}