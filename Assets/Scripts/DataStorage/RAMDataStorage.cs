﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DataStorage
{
    public class RAMDataStorage : MonoBehaviour, IDataStorage
    {
        private Dictionary<string, object> _storage = new Dictionary<string, object>();

        public void SetValue(string key, int value)
        {
            SetValue(key, (object)value);
        }

        public void SetValue(string key, float value)
        {
            SetValue(key, (object)value);
        }

        public void SetValue(string key, string value)
        {
            SetValue(key, (object)value);
        }

        public void SetValue(string key, bool value)
        {
            SetValue(key, (object)value);
        }

        public string GetString(string key)
        {
            return GetValue<string>(key);
        }

        public int GetInt(string key)
        {
            return GetValue<int>(key);
        }

        public float GetFloat(string key)
        {
            return GetValue<float>(key);
        }

        public bool GetBool(string key)
        {
            return GetValue<bool>(key);
        }

        public bool HasKey(string key)
        {
            return _storage.ContainsKey(key);
        }

        public void DeleteKey(string key)
        {
            _storage.Remove(key);
            GC.Collect();
        }

        private void SetValue(string key, object value)
        {
            if (_storage.ContainsKey(key))
            {
                _storage[key] = value;
            }
            else
            {
                _storage.Add(key, value);
            }
        }

        private T GetValue<T>(string key)
        {
            return (T)_storage[key];
        }
    }
}
