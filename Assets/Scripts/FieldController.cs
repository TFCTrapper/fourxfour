﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class FieldController : MonoBehaviour, IInitializable
{
    public List<List<CardView>> field { get; private set; }
    public CardView selectedCard { get; set; }
    public int pairsCount { get; set; }

    [SerializeField]
    private Vector2Int _size;
    [SerializeField]
    private GameObject _cardPrefab;

    private Game _game;
    private List<Card> _cards;

    public void Initialize(dynamic data)
    {
        _cards = data.cards;
        _game = data.game;
        CreateField();
    }

    public void Save()
    {
        _game.dataStorage.SetValue("FieldSizeX", _size.x);
        _game.dataStorage.SetValue("FieldSizeY", _size.y);
        for (int i = 0; i < _size.x; i++)
        {
            for (int j = 0; j < _size.y; j++)
            {
                string key = "Card" + i.ToString() + "_" + j.ToString();

                if (_game.dataStorage.HasKey(key))
                {
                    _game.dataStorage.DeleteKey(key);
                }

                if (field[i][j] != null)
                {
                    int cardIndex = _cards.IndexOf(field[i][j].card);
                    _game.dataStorage.SetValue(key, cardIndex);
                }
            }
        }
        _game.dataStorage.SetValue("PairsCount", pairsCount);
    }

    public void LoadFromSave()
    {
        _size = new Vector2Int(
            _game.dataStorage.GetInt("FieldSizeX"),
            _game.dataStorage.GetInt("FieldSizeY")
        );

        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        foreach (var column in field)
        {
            column.Clear();
        }
        field.Clear();

        Vector2 cardSize = _cardPrefab.GetComponent<RectTransform>().sizeDelta;

        for (int i = 0; i < _size.x; i++)
        {
            List<CardView> column = new List<CardView>();
            Transform columnTransform = Instantiate(_game.emptyPrefab, Vector3.zero, Quaternion.identity, transform).GetComponent<Transform>();
            columnTransform.name = "Column" + i;
            int cardIndex = 0;
            for (int j = 0; j < _size.y; j++)
            {
                string key = "Card" + i.ToString() + "_" + j.ToString();
                if (!_game.dataStorage.HasKey(key))
                {
                    column.Add(null);
                    continue;
                }
                cardIndex = _game.dataStorage.GetInt(key);

                Vector3 worldPosition = new Vector3(
                    -cardSize.x * (_size.x - 1) / 2f + cardSize.x * i,
                    -cardSize.y * (_size.y - 1) / 2f + cardSize.y * j,
                    0f);

                CardView cardView = Instantiate(_cardPrefab, worldPosition, Quaternion.identity, columnTransform).GetComponent<CardView>();
                Vector2Int position = new Vector2Int(i, j);
                cardView.Initialize(new { card = _cards[cardIndex], position, game = _game });
                column.Add(cardView);
            }
            field.Add(column);
            pairsCount = _game.dataStorage.GetInt("PairsCount");
            GC.Collect();
        }

        Camera.main.orthographicSize = cardSize.y * _size.y / 2f;
    }

    private void CreateField()
    {
        Queue<int> fieldFirstHalf = new Queue<int>();

        field = new List<List<CardView>>();

        Vector2 cardSize = _cardPrefab.GetComponent<RectTransform>().sizeDelta;

        for (int i = 0; i < _size.x; i++)
        {
            List<CardView> column = new List<CardView>();
            Transform columnTransform = Instantiate(_game.emptyPrefab, Vector3.zero, Quaternion.identity, transform).GetComponent<Transform>();
            columnTransform.name = "Column" + i;
            int cardIndex = 0;
            for (int j = 0; j < _size.y; j++)
            {
                Vector3 worldPosition = new Vector3(
                    -cardSize.x * (_size.x - 1) / 2f + cardSize.x * i,
                    -cardSize.y * (_size.y - 1) / 2f + cardSize.y * j,
                    0f);

                CardView cardView = Instantiate(_cardPrefab, worldPosition, Quaternion.identity, columnTransform).GetComponent<CardView>();
                
                if (i * _size.y + j < _size.x * _size.y / 2)
                {
                    cardIndex = UnityEngine.Random.Range(0, _cards.Count);
                    fieldFirstHalf.Enqueue(cardIndex);
                }
                if (i * _size.y + j == _size.x * _size.y / 2)
                {
                    Queue<Card> queue = new Queue<Card>();
                    queue.OrderBy(x => UnityEngine.Random.value);
                    fieldFirstHalf = new Queue<int>(fieldFirstHalf.OrderBy(x => UnityEngine.Random.value).ToList());
                }
                if (i * _size.y + j >= _size.x * _size.y / 2)
                {
                    cardIndex = fieldFirstHalf.Dequeue();
                }
                Vector2Int position = new Vector2Int(i, j);
                cardView.Initialize(new {card = _cards[cardIndex], position, game = _game});
                column.Add(cardView);
            }
            field.Add(column);
            pairsCount = _size.x * _size.y / 2;
            GC.Collect();
        }

        Camera.main.orthographicSize = cardSize.y * _size.y / 2f;
    }
}
