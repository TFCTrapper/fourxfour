﻿using DataStorage;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class Game : MonoBehaviour
{
    public FieldController fieldController { get => _fieldController; private set => _fieldController = value; }
    public UIController uiController { get; private set; }
    public GameObject emptyPrefab { get => _emptyPrefab; private set => _emptyPrefab = value; }
    public IDataStorage dataStorage { get; private set; }
    public Color defaultCardColor { get => _defaultCardColor; private set => _defaultCardColor = value; }
    public Color selectedCardColor { get => _selectedCardColor; private set => _selectedCardColor = value; }
    public float levelTime { get => _levelTime; private set => _levelTime = value; }
    public float currentLevelTime { get; private set; }
    public bool isPlaying { get; private set; } = true;

    [SerializeField]
    private string _cardsDirectory;
    [SerializeField]
    private FieldController _fieldController;
    [SerializeField]
    private GameObject _emptyPrefab;
    [SerializeField]
    private Color _defaultCardColor;
    [SerializeField]
    private Color _selectedCardColor;
    [SerializeField]
    private float _levelTime;

    private List<Card> _cards;

    public void Match(CardView card0, CardView card1)
    {
        card0.Kill();
        card1.Kill();
        fieldController.pairsCount--;
        CheckResult();
    }

    public void CheckResult()
    {
        if (fieldController.pairsCount <= 0)
        {
            isPlaying = false;
            uiController.panelsManager.Show("Result", "You win!");
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene("Main");
    }

    public void Close()
    {
        Application.Quit();
    }

    public void SaveGame()
    {
        dataStorage.SetValue("CurrentLevelTime", currentLevelTime);
        fieldController.Save();
        dataStorage.SetValue("IsSaved", true);
        uiController.panelsManager.GetPanel<MainPanel>().UpdateButtons();
    }

    public void LoadGame()
    {
        currentLevelTime = dataStorage.GetFloat("CurrentLevelTime");
        fieldController.LoadFromSave();
    }

    private void Awake()
    {
        UpdateCards();
        uiController = FindObjectOfType<UIController>();
        uiController.Initialize(this);
        _fieldController.Initialize(new {cards = _cards, game = this});
        currentLevelTime = levelTime;
    }

    private void Update()
    {
        if (isPlaying)
        {
            currentLevelTime -= Time.deltaTime;
            if (currentLevelTime <= 0f)
            {
                isPlaying = false;
                uiController.panelsManager.Show("Result", "You lose");
            }
        }
    }

    [Inject]
    private void Inject(IDataStorage dataStorage)
    {
        this.dataStorage = dataStorage;
    }

    private void UpdateCards()
    {
        _cards = Resources.LoadAll<Card>(_cardsDirectory).ToList();
    }
}
