﻿using UnityEngine;
using DataStorage;

public interface IInitializable
{
    void Initialize(object data);
}

public interface IKillable
{
    void Kill();
}

public interface ISelectable
{
    void Select();
    void Unselect();
}