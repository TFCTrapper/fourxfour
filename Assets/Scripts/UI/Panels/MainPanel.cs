﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainPanel : Panel
{
    [SerializeField]
    private Text _timerText;
    [SerializeField]
    private Button _saveButton;
    [SerializeField]
    private Button _loadButton;
    [SerializeField]
    private Button _closeButton;

    public override void Initialize(object game)
    {
        _game = (Game)game;

        _saveButton.onClick.AddListener(OnSaveClick);
        _loadButton.onClick.AddListener(OnLoadClick);
        _closeButton.onClick.AddListener(OnCloseClick);

        _loadButton.interactable = _game.dataStorage.HasKey("IsSaved");
    }

    public void UpdateButtons()
    {
        _loadButton.interactable = _game.dataStorage.HasKey("IsSaved");
    }

    private void Update()
    {
        _timerText.text = Mathf.Floor(_game.currentLevelTime).ToString();
    }

    private void OnDestroy()
    {
        _saveButton.onClick.RemoveListener(OnSaveClick);
        _loadButton.onClick.RemoveListener(OnLoadClick);
        _closeButton.onClick.RemoveListener(OnCloseClick);
    }

    private void OnSaveClick()
    {
        _game.SaveGame();
    }

    private void OnLoadClick()
    {
        _game.LoadGame();
    }

    private void OnCloseClick()
    {
        _game.Close();
    }
}
