﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Panel : MonoBehaviour, IInitializable
{
    protected Game _game;

    public virtual void Initialize(object game)
    {
        _game = (Game)game;
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Show(string message)
    {
        Show();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
