﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelsManager : MonoBehaviour
{
    [SerializeField]
    private string _panelPrefabsPath;
    [SerializeField]
    private string _panelNamePostfix;
    [SerializeField]
    private List<string> _panelNamesToLoad;
    [SerializeField]
    private string _firstPanel;

    private List<Panel> _panels = new List<Panel>();
    private Panel _activePanel;

    public void Initialize(object mainController)
    {
        foreach (string panelName in _panelNamesToLoad)
        {
            GameObject prefab = Resources.Load<GameObject>(_panelPrefabsPath + "/" + panelName + _panelNamePostfix);
            Panel panel = Instantiate(prefab, transform).GetComponent<Panel>();
            panel.gameObject.name = prefab.name;
            panel.Initialize(mainController);
            panel.gameObject.SetActive(false);
            _panels.Add(panel);
        }

        Show(_firstPanel);
    }

    public void Show(string panelName, string message = null)
    {
        if (_activePanel != null)
        {
            _activePanel.Hide();
        }

        foreach (Panel panel in _panels)
        {
            if (panel.gameObject.name.Equals(panelName + _panelNamePostfix))
            {
                if (message == null)
                {
                    panel.Show();
                }
                else
                {
                    panel.Show(message);
                }
                _activePanel = panel;
            }
        }
    }

    public void HideActivePanel()
    {
        _activePanel.Hide();
    }

    public T GetPanel<T>() where T : Panel
    {
        foreach (Panel panel in _panels)
        {
            if (panel.GetType().Equals(typeof(T)))
            {
                return (panel as T);
            }
        }
        return null;
    }
}
