﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultPanel : Panel
{
    [SerializeField]
    private Text _messageText;
    [SerializeField]
    private Button _restartButton;
    [SerializeField]
    private Button _closeButton;

    public override void Initialize(object game)
    {
        _game = (Game)game;

        _restartButton.onClick.AddListener(OnRestartClick);
        _closeButton.onClick.AddListener(OnCloseClick);
    }

    public override void Show(string message)
    {
        base.Show(message);
        _messageText.text = message;
    }

    private void OnDestroy()
    {
        _restartButton.onClick.RemoveListener(OnRestartClick);
        _closeButton.onClick.RemoveListener(OnCloseClick);
    }

    private void OnRestartClick()
    {
        _game.Restart();
    }

    private void OnCloseClick()
    {
        _game.Close();
    }
}
