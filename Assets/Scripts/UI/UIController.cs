﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour, IInitializable
{
    public PanelsManager panelsManager;

    public void Initialize(object game)
    {
        panelsManager.Initialize(game);
    }
}